ODILO Senior DevOps Engineer Recruitment Test
==================================

Thank you for taking the time to do our technical test. It consists of two parts:

* [A technical test](#technical-test)
* [A few technical questions](#technical-questions)


In case you decided to use a public Git service such as GitHub, GitLab or Bitbucket to create a code repository please do not forget to make it private.
Once you have completed the technical test we would like you to submit by uploading the relevant zip file to a shared Google Drive folder provided by ODILO. In order to obtain the URL for this folder, please supply your Gmail or Google-based email address to the ODILO member of staff who assigned you the test.

Please make this a **single** zip file named `{your_first_name}-{your_last_name}-devops-senior.zip` containing:

1. A single markdown file with the answers to the technical questions
2. One folder containing the technical test


## Technical Test

1. Your task is to create an automated CI/CD pipeline for a REST API service running on AWS ECS with the following the requirements depicted below.
3. With this test, we want to see your ability to create an entire infrastructure from scratch as well as your skills as a system administrator.

### Requirements

* In your solution please emphasize on readability, maintainability and DevOps methodologies. We expect a clear way to recreate your setup.
* You can use any "Hello-world" project written in any language or framework of your choice as your REST API service.
* All the AWS resources should be provisioned as a code using Terraform.
* Your Docker image creation and provisioning should be automated as a code as well (you can use Packer + Ansible for that).
* You can use any CI/CD platform for the pipeline.
* You must implement a blue/green deployment with zero downtime.

### Hints

* Create a new testing AWS Account or use a different region with no other resources.
* Use Packer to automate the creation of the Docker image with Ansible as the provisioner.
* Use AWS ECR as your repository for images (it will easily integrate with ECS).
* If you decide to use CircleCI, leverage the use of community ORBs to automate operations with ECS/ECR. Use Ansible for any other automation.
* You will end up having a service running on ECS and a load balancer (with probably two Target Groups).

### What we expect

* A clean bare minimum working infrastructure is preferred than a full blown solution pieced together with scissors, rope and duct tape.
* Do not skip security considerations.
* You should provide clear instructions on how to use the code you have provided. The clarity and precision of these instructions - and the ease with which the interviewers can execute them - will be a key part of the assessment. Please create a README file detailing said instructions. Please also use this file for listing any additional comments or observations you might want to share about your submission.
* Do not waste time in reinventing the wheel, select and reuse existing solutions and code repositories if necessary.

### Bonus Points

* If you can use CircleCI for the CI/CD pipeline
* If you can document all aspects of your code, in the README and within the code itself.


# Technical questions

Please answer the following questions in a markdown file called `Answers to technical questions.md`.

1. We understand that, beyond this bare minimum working infrastructure, you would have probably come up with way lots of ideas or improvements. What would you add to your solution if you had more time?
2. If you didn't spend much time on the coding test, please explain what tools, framework or approaches you would add to throughly test the project's pipeline.
3. What underlying computing engine did you choose for ECS and why (Fargate or EC2)? Can you spot any downsides when using that engine?


#### Thanks for your time, we look forward to hearing from you!
- The unique [ODILO](https://www.odilo.es/careers/) team