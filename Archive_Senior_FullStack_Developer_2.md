# ODILO Senior FullStack Developer V.2 for Archive Team Recruitment Test

## El problema

Necesitamos el desarrollo de un microservicio que resolverá, dada una cadena de texto, todos los palíndromos que se encuentren en ésta. Por ejemplo, la frase "A cavar a Caravaca" se lee igual en un sentido o en otro, pero igualmente tiene varios palíndromos (no consecutivos) "A ca**var** a Ca**rav**aca" (var). Se pide la creación de una API en un microservicio que tenga los siguientes métodos:

* Login de usuario y cambio de contraseña.
* Método para averiguar si una cadena de caracteres es un palíndromo. Responderá verdadero/falso. Por ejemplo, debería indicar si la sentencia "A cavar a Caravaca" es un palíndromo pero también si la cadena de caracteres "aarrggbbccccbbggrraa" lo es.
* **(Opcional)** Método para que dada una cadena de caracteres que sea capaz de devolver todos los palíndromos, consecutivos o no, que ésta contiene.
* **(Opcional)** Método para añadir "palabras ignorables" a la hora de detectar palíndromos. Por ejemplo, si indico que se debe ignorar la palabra "caravaca" entonces el método del 2º punto me responderá cuando pase la frase "A cavar a Caravaca" que no es un palíndromo, pero además deberá informar de la razón (porque se ha ignorado la palabra "caravaca").
* Todos aquellos otros métodos que creas que son necesarios para el buen funcionamiento del microservicio.

Utilizaremos Java >= 8 para la implementación y para la base de datos te recomendamos una Base de datos M2 embebida, que te permitirá desarrollar todo más rápido.

## El entregable
* El proyecto Java que se pueda ejecutar y con la mínima funcionalidad pedida.
* Fichero SQL con el código necesario para crear las tablas e insertar los datos mínimos para que funcione la aplicación (si fuese necesario).
* Los detalles de configuración o cualquier otro detalle que consideres interesantes.

Puesto que se pide una pantalla de login de usuario, para simplificar te sugiero usar páginas HTML planas, no es necesario que sea nada muy sofisticado, pero si lo consideras oportuno puedes llegar a desarrollar lo que consideres necesario.

**Se valorará positivamente que se tomen medidas de seguridad en la aplicación, y cualquier otra mejora al problema base que consideres oportuna para el buen funcionamiento de la aplicación. También se valorará el uso de patrones de diseño y el uso de principios S.O.L.I.D**

Una vez terminado el proyecto realizar un envío con el zip con toda la documentación al correo de la persona que te pasó esta prueba técnica.

¡Mucha suerte!