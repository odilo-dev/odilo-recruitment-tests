# ODILO Senior Mobile Challenge 

## PRUEBA DE CÓDIGO

Hay que Desarrollar una pequeña app en Swift que cubra los siguientes requisitos:

### Parte1:

* Botón 1 en el centro de la vista que arranque la descarga de varios ficheros.

* El botón para iPhone estará anclado a la derecha e izquierda de la vista a una distancia de 16px y tendra una altura de 40px. Para iPad tendrá un ancho fijo de 200 y una altura de 40px y estara centrado en la vista.


* Solo habrá <b>una descarga de fichero simultanea </b>.

* Si el usuario se va a segundo plano la app debe seguir con la descarga del resto de ficheros

* Cuando finalice cada descarga debe mostrar una notificación al usuario

Guardar cada descarga en disco

Ficheros disponibles :

https://filesamples.com/samples/audio/mp3/Symphony No.6 (1st movement).mp3

https://filesamples.com/samples/audio/mp3/sample4.mp3

https://filesamples.com/samples/audio/mp3/sample3.mp3

https://filesamples.com/samples/audio/mp3/sample2.mp3

https://filesamples.com/samples/audio/mp3/sample1.mp3





### Parte 2 :

Usando un único fichero

* Botón 2 (mismos tamaños y disposición que botón 1 pero situado debajo de este a una distancia de 16px) y que arranque 50 descargas del fichero 1 ( https://filesamples.com/samples/audio/mp3/Symphony No.6 (1st movement).mp3)

* <b>Limite de cinco descargas de fichero simultaneas</b>

* Si el usuario se va a segundo plano debe seguir con el proceso que quede pendiente

* Cuando finalice cada descarga debe mostrar una notificación al usuario

* Guardar cada descarga en disco

---
### Notas
1.  Detallar la solución propuesta en un documento e indicar los posibles limitaciones o problemas que pudiéramos encontrarnos (si los hubiera).

2. Añade lo que consideres oportuno y pueda resultar interesante en la revisión de la prueba.




#### Tecnologías

Swift

##### Consideraciones

1. La calidad del código es fundamental. Un punto a favor es la forma de estructurar el código y que sea fácilmente extendible, que cumpla con las mejores practicas de diseño de sw
2. Aplicar patrón VIPER
3. Test unitarios



## Instrucciones de entrega

- Plazo de 7 días, 1 semana
- Entrega la solución (código fuente) en un fichero ZIP con las notas, y documentos que consideres al correo electrónico de la persona que te contactó para la prueba.
