# README #

This repository includes a collection of documents describing technical tests used in our recruitment processes at ODILO.

### Roles ###

* [DevOps Senior Engineer](Senior_DevOps_Engineer.md)
* [Archive Senior FullStack Developer](Archive_Senior_FullStack_Developer.md)
* [Archive Senior FullStack Developer V2](Archive_Senior_FullStack_Developer_2.md)

