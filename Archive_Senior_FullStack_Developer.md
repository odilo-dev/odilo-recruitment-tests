# ODILO Senior FullStack Developer for Archive Team Recruitment Test

## El problema

Necesitamos el desarrollo de un microservicio que firmará desasistidamente documentos para diferentes clientes. Este microservicio conformará una aplicación completa con entidad propia que permitirá acceso a cada usuario a su “espacio personal” que contendrá su certificado de persona física o jurídica (inaccesible para el resto de usuarios), a través del espacio personal de cada usuario podrá subir su certificado y comprobar si éste está subido. Para éste servicio necesitaremos las siguientes pantallas:
* De login de usuario.
* De subida de certificados (y la contraseña de este certificado, necesaria para firmar).
* Cualquier otra pantalla que se considere necesaria.

Igualmente se requiere un endpoint */firma* que reciba un binario como cadena de texto y que realice la firma sobre dicho binario, devolviendo ésta.
Utilizaremos Java >= 8 para la implementación y para la base de datos te recomendamos una Base de datos PostgresSQL (https://www.postgresql.org/download/) que es gratuita, estructurada y muy similar a Oracle en su uso, pero puedes usar otra si así lo consideras.

## El entregable
* El proyecto Java que se pueda ejecutar y con la mínima funcionalidad pedida.
* Fichero SQL con el código necesario para crear las tablas e insertar los datos mínimos para que funcione la aplicación (si fuese necesario).
* Una reflexión escrita de los cambios que se harían para que el servicio fuese **Multitenant** (configuración de la base de datos, clases que se implementarían, cómo distinguirías el tenant…) o la implementación del servicio como multitenant.
* Los detalles de configuración que consideres interesantes.

Para simplificar te sugiero usar páginas HTML planas, no es necesario que sea nada muy sofisticado, pero si lo consideras oportuno puedes desarrollar lo que consideres. Igualmente, el servicio */firma* con que reciba datos y devuelva algo del estilo #firma#<y lo que se haya enviado> es suficiente, no es necesario que “implementes ningún proceso de firma real” (no entra en el objetivo y además es más complejo de lo que parece), pero si es importante que queden implementadas las clases por las que pasaría hasta llegar al punto donde se haría “Obj.firma()”. Para la subida de certificados basta con que se simule que el certificado es un fichero de texto vacío; el almacenamiento de éstos puede ser el que consideres oportuno.

**Se valorará positivamente que se tomen medidas de seguridad en la aplicación, y cualquier otra mejora al problema base que consideres necesaria para el buen funcionamiento de la aplicación. También se valorará el uso de patrones de diseño y la limpieza del código.**

Una vez terminado el proyecto realizar un envío con el zip con toda la documentación al correo de la persona que te pasó esta prueba técnica.

¡Mucha suerte!