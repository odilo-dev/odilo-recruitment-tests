## Senior Backend Java Engineer (Second version)

> **Note:** Please read all the requirements before starting.


# Context
You are working at a company that distributes geek products and information to users through a subscription model. The time has come to begin development, and the product team has sent you a document containing the requirements. Study the requirements carefully and implement them based on your experience and best practices.
Keep in mind that you are the technical expert! The product team may not be familiar with best practices for building REST APIs, writing clean code, etc.

### Tech Requirements

- >Java >= 1.8
- > Spring Boot
- > Maven 
- > Unit and integration Tests (more than 25% coverage). Add the necessary dependency to check the coverage


### Friendly tips

- > Don't be afraid to use logs! It is important to record all the actions performed by the system
- > Feel free to apply any type of architecture (layered,hexagonal,events,etc), pattern (Builder,Factory,Circuit Breaker,etc) or element/libraries (H2,lombok, cache, docker, testcontainer, swagger,JWT, etc)
  
Just make sure to create a `readme.md` to let us know `where`, `what` and `why`! (be creative whenever you can)

----
# Task: Do Androids Dream of Electric Sheep? 

Hey! Welcome to the backend of `OdiGeek`. As you may have been told, our business is to provide geek-related information and products to our `users` through a `subscription` (after all, we need to fill our pockets somehow). We need you to build a system that will likely be used by the Frontend team, and it should allow us to:

- Manage `users`: 
  - The **_sales team_** representative told us they only need the following data (for the sales campaigns):
    - Name
    - Email
    - Phone (Valid, with country codes)
  - The **_BI team_** has mentioned they would like more information for data analysis (we have a meeting pending to see if they need more data):
    - Age (users must be over 18 to register)
    - City of residence
  - The **_finance team_** requires information related to payments (card, bank account, etc.)
  
- Manage `subscriptions`:
  - Title (must be unique within the category)
  - Category (a subscription could belong to multiple categories)
  - Price
  - Description
  - Content (for now, we have 2 types):
    - Figures (They will be sent to the client once the subscription has been paid)
    - Data (ID?)

There is a very high probability that we will need to copy the data from our database to the databases of the different teams since they need to process the data. We're open to suggestions!

We have closed an agreement with two major suppliers: **_DragonBall_** and **_Star Wars_**! We believe that soon we may be able to get these suppliers to allow us to resell figures to our users!!

The suppliers have provided us with the API we need to consume in order to obtain the necessary information and prepare ourselves to acquire the figures:

- https://web.dragonball-api.com/
- https://swapi.dev/

Just keep in mind that they turn off the API on the weekends to deploy new changes and to run some updates on their catalog.

We're going **_all-in_**! We will prepare `several servers` in different `regions`! We need that whenever we deploy an instance of our application, some default subscriptions are created, depending on the main theme configured (profile). 


**_If profile = Dragon Ball:_** 

Call the client's API (https://dragonball-api.com/api-docs#/Characters/CharactersController_findAll) and create 2 subscriptions in our database: one for `Humans` and another for `Saiyans`, using the information from the first 10 items returned by the API (we are not interested in certain data like the ID and the image).

Example: 
```json
{
   "_ID": 1,
   "Title": "DBZ_HUMANS",
   "Category": "ANIME",
   "Price": 10.0,
   "Currency": "USD",
   "Description": "Get all the DBZ Human data and figures NOW!",
   "Content":{
      "DATA":[
         {"id":1,"Description":"10 strongest humans!","Content": "https://dragonball-api.com/api/characters?page=1&limit=10&race=Human"}
      ],
      "FIGURES": [
         {"id":1,"reference":"HUMAN_001", "description":"Krillin 50cm bold"},
         {"id":2,"reference":"HUMAN_002","description":"Tenshinhan 20cm dead"}
      ]
   }
}
```

**_If profile = Star Wars:_**

Call the client's API (https://swapi.dev/api/people && https://swapi.dev/api/planets) to create 2 subscriptions: `people` and `planets`, same as the "Dragon ball" profile, we will get the first 10 items. 

**_If profile = Dragon Ball && Star Wars:_**

Apply both logic (4 subscriptions)

**_If profile is NOT Dragon Ball OR Star Wars:_**

Don't create any default subscriptions



To keep in mind:

- Each subscription topic has a base cost that may change frequently (depending on its popularity at the time of subscription). What does this mean? Today a subscription may cost 5€, but it could increase to 10€, or even go on sale for 2€! A geek specialist will be responsible for uploading price changes in advance. 
- We need to create a way for the specialist to create offers in the system (with a start date), so they can specify whether the offer will apply to a specific title, a category, or the entire catalog.
  Example (`Offer`):
```json
{
   "_ID": 1,
   "Title": "ANIME_50%_OFF",
   "Description": "50% OFF on any ANIME",
   "Rules": {
      "Price": 0.5,
      "Currency": "USD"
   },
   "Scope": {
      "Title": null,
      "Category": ["ANIME"]
   },
   "Starts": "2024-11-20T16:28:33.480Z",
   "End": "1734712113480"
}
```
- The system should check every day if there are any offers to apply and update the price of items (if conditions are met). Keep in mind that one day, we may have a lot of subscriptions!
- User should have access to their subscription data any time they want!

Example
 ```json
{
  "id": 1, 
  "name": "Geek",
  "username/email": "user@foo.com",
  "dateOfBirth": "2000-01-01", 
   "subscriptions": [
      {
         "_ID": 1,
         "Title": "DBZ_HUMANS",
         "Category": "ANIME",
         "Price": 10.0,
         "Currency": "USD",
         "Description": "Get all the DBZ Human data and figures NOW!",
         "Content":{
            "DATA":[
               {"id":1,"Description":"10 strongest humans!","Content": [
                  {
                     "id": 4,
                     "name": "Bulma",
                     "ki": "0",
                     "maxKi": "0",
                     "race": "Human",
                     "gender": "Female",
                     "description": "Bulma es la protagonista femenina de la serie manga Dragon Ball y sus adaptaciones al anime Dragon Ball, Dragon Ball Z, Dragon Ball Super y Dragon Ball GT. Es hija del Dr. Brief y su esposa Panchy, hermana menor de Tights y una gran amiga de Son Goku con quien inicia la búsqueda de las Esferas del Dragón. En Dragon Ball Z tuvo a Trunks, primogénito de quien sería su esposo Vegeta, a su hija Bra[3] y su hijo adulto del tiempo alterno Trunks del Futuro Alternativo.",
                     "affiliation": "Z Fighter"
                  },
                  {
                     "id": 11,
                     "name": "Krillin",
                     "ki": "1.000.000",
                     "maxKi": "1 Billion",
                     "race": "Human",
                     "gender": "Male",
                     "description": "Amigo cercano de Goku y guerrero valiente, es un personaje del manga y anime de Dragon Ball. Es uno de los principales discípulos de Kame-Sen'nin, Guerrero Z, y el mejor amigo de Son Goku. Es junto a Bulma uno de los personajes de apoyo principales de Dragon Ball, Dragon Ball Z y Dragon Ball Super. Aparece en Dragon Ball GT como personaje secundario. En el Arco de Majin Boo, se retira de las artes marciales, optando por formar una familia, como el esposo de la Androide Número 18 y el padre de Marron.",
                     "affiliation": "Z Fighter"
                  }
               ]}
            ],
            "FIGURES": [
               {"id":1,"reference":"HUMAN_001", "description":"Krillin 50cm bold"},
               {"id":2,"reference":"HUMAN_002","description":"Tenshinhan 20cm dead"}
            ]
         }
      }
   ]
}
```

- Also, even on weekends, we need the supplier's catalogs to be available for the geek specialist to review!  

---


### Delivery
- When you're done, send a ZIP file with the project source code + the Postman collection to the people who assigned you the task.
- The deadline usually is 1 week, 7 days, let us know if you need more time or if you want to ask something!



### Tech design interview
- If you are selected, there will be an interview to review the technical design and implementation, where we will analyze the code and the candidate will showcase a Postman collection for testing in a live demo. We will ask questions, and it will be a two-way conversation.
- Don't forget to prepare the Postman collection, the docker images, or what ever you need to show us the app running! ;) 



