## Prueba de código Senior Backend Engineer Java
  
> **Nota:** Por favor, lee todos los requisitos antes de comenzar con >Se valorará positivamente la validación de las entradas y el control de errores. 


#### Contexto
Esto es un prototipo de proyecto para crear un microservicio de registro y login de usuarios, dentro del ámbito de una aplicación más grande de gestión expuesta al público.

Lo que se espera del candidato es una serie de decisiones técnicas e implementaciones acertadas en linea con el contexto de dicha aplicación.

### Requisitos

1. Proporcionar un docker file para la construcción de la imagen, debe ser compatible con windows, mac y linux.
	* Añade un .md comentando las principales decisiones técnicas
2. Usa Java >= 1.8 y Spring Boot (una versión reciente a tu elección)
3. Utiliza Spring Data JPA para interactuar con algún sistema SQL  (Oracle express, Postgre,..)
4. Añade las dependencias necesarias para Redis como caché de los datos de usuarios (en local en el contenedor)
5. Preferiblemente utiliza Maven como gestor de proyecto
6. Añade test unitarios en tu proceso de desarrollo, a tu elección la libreria.
7. Se debe implementar un servicio api-Rest para registro y autenticación de usuarios, lo más RESTful posible basado en estos dos entidades principales:

#### Entidad USER

Tendrá una pinta tal que así:
 ```json
{
  "id": 1,
  "username": "usuarioejemplo",
  "password": "passwordEjemplo",
  "email": "usuario@foo.com",
  "dateOfBirth": "2000-01-01"
}
```

 #### Entidad ROLE
Habrá 2 valores que de alguna manera se deben "setear" en el arranque por primera vez de la aplicación
```json
[
  {
    "id": 1,
    "name": "ROLE_ADMIN"
  },
  {
    "id": 2,
    "name": "ROLE_USER"
  }
]
```

A los usuarios normales que se pueden registrar se les dará el rol "ROLE_USER", que solo puede acceder a sus propios datos
Habrá un usuario único administrador que estará en el "ROLE_ADMIN" y ese tendrá visibilidad total.


9. Ten en cuenta la seguridad, utiliza JSON Web Tokens para los tokens de sesión que luego se envían al cliente 
10. Politica de seguridad robusta a tu elección para contraseñas y mecanismos de reintentos, operaciones autorizadas..
11. Spring Security para autenticación y autorización. Decide que controles tiene sentido implementar para que los usuarios no tengan acceso a información que no deben
12. Chequea que solo pueden registrarse los usuarios mayores de 18 años



 #### Endpoints
  Para el caso de la prueba nos quedamos con este subconjunto de endpoints:

| Endpoint                       | Especificación                                                            |  
|--------------------------------|---------------------------------------------------------------------------|
|`POST /odilo/api/register`      | Registra un nuevo usuario  
|`POST /odilo/api/login`               | Permite a un usuario iniciar sesión     |
|`GET /odilo/api/users/{userId}`            | Obtiene la información detallada de un usuario especifico. (proveniente de caché) 
| `GET /odilo/api/admin/users/` | Devuelve la lista de todos los usuarios (información proveniente de caché)
| `DELETE /odilo/api/users/{userId}`               |Elimina un usuario especifico (a si mismo)
|`PUT /odilo/api/admin/users/{userID}` | Cambia la password de un usuario|




<b>OPCIONAL, para ganar más puntos: </b>

13. Añade un mecanismo de suscripción para que diferentes microservicios "clientes" de este servicio de registro y login, pertenecientes a la aplicación ACME puedan recibir updates de todas las operaciones realizadas por un usuario (registros, login,..). Ten en cuenta que el número de servicios podría incrementarse a futuro y podrían ir activandose o desactivandose servicios



#### Consideraciones
* La información de usuarios la debe recuperar de la cache
* Hipoteticamente esto estará expuesto a mucho usuarios y alto tráfico




### Entrega
* Cuando termines, envía un ZIP con los fuentes del proyecto + colección postman a las personas que te enviaron la tarea
* El plazo es de 1 semana, 7 días.



### Entrevista de diseño técnico
Si quedas seleccionado, habrá una entrevista de revisión del diseño técnico e implementación donde analizaremos el código y el candidato mostrará en ejecución en directo una coleccion de postman para las pruebas , haremos preguntas y será una conversación bidireccional .

No olvides prepararar la coleccion de postman para las pruebas



