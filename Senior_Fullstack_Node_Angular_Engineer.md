ODILO Senior Fullstack MEAN Engineer Test
=========================================

### PRUEBA DE CÓDIGO 


En esta prueba se valorará la resolución de los enunciados y sobre todo las buenas prácticas de código y el diseño de interfaz, responsabilidades..

Se pueden añadir las dependencias necesarias, así como el uso de componentes externos.

También puedes refactorizar el código ofrecido o incluso modificar el modelo, si lo ves conveniente.



#### Club de lectura 


Se quiere implementar una aplicación de un club de lectura, donde usuarios podrán entrar a esta plataforma y navegar por los diferentes clubs de lectura, donde podrás encontrar cientos/miles de clubs potencialmente. 

Partimos entonces, de usuarios y clubs como modelo de datos. 

Este proyecto parte con un cliente en angular y la parte servidor en node 12 y mongodb, ambos funcionales.

El arquetipo funcional está en la carpeta resources/Senior_Fullstack_MEAN. Descargalo en un repo personal y trabaja sobre esa versión.

<b>Requerimiento no funcional: Se asume que el proyecto tendría altas volumetrías de usuarios y clubs. Principalmente habrá clubs con muchos usuarios, pero podría darse el caso también de los usuarios fueran invitados a muchos clubs diferentes</b>


#### Implementar:

* Listar todos los clubs de lectura en la home, diferenciando entre los que sigue el usuario y los que no sigue, es una acción reversible.

* Permitir a un usuario seguir un club de lectura concreto y reflejarlo en la interfaz

Se valorará:

* Mecanismos de seguridad , como JWT.
* control errores, excepciones adecuado
* Paginaciones
* Responsabilidad del código, cohesión, arquitectura del proyecto
* No usar bootstrap o similares en el front
* Añadir test 
* Diseño adecuado de las colecciones


#### Opcional:

- Piensa como montarías un sistema de estadísticas, que de alguna manera deje traza de las operaciones que se realicen para gestionar usuarios, clubs y su relación N x M

- Otras mejoras que se te ocurran


### Instrucciones de entrega

* Plazo de una semana, 7 días.
* Cuando termines:

    - Comparte un drive con el fichero {your_first_name}-{your_last_name}-fullstack.zip

    - O bien nos envias un mail con el código fuente en el mismo formato

En cualquier caso, a las personas que te envíemos el mail de la práctica nos incluyes en el mail/invitación.

