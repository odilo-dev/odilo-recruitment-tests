ODILO Mobile Challenge Recruitment Test
==================================

Realizar una aplicación en Java o Kotlin con estos requerimientos :

1 - Extraer 20 superheroes aleatorios de la api : https://superheroapi.com 
Token : 1738436099652678

2 - Mostrar una lista con los superheroes aleatorios con esta información :
Foto (image) 
Id
Nombre (name)

3 - Dar opción de borrar superheroes de la lista.

4 - Al presionar en un superhéroe se debe dirigir al usuario a otra vista donde se mostrará más detalle :

Image
Full Name 
Strength 
Power
Place of Birth 
Gender
Height 
Aliases

* Disponer la información con el UI que se estime oportuno (Diseño libre)
* Pensar en la arquitectura y diseño del modelo e intentar implementarlo lo mas modular posible.
* La calidad del código es fundamental. Un punto a favor es la forma de estructurar el código y que sea fácilmente extendible, que cumpla con las mejores practicas de diseño de sw, patrones y arquitectura . El objetivo es que tu código sea fácil de modificar y entender por los compañeros.
* Piensa en la posibilidad de introducir algún Unit Test.
* Para más información de la Api : https://superheroapi.com
* Tiempo máximo estimado : 3 días
* Entrega la solución en un fichero ZIP con las notas, comentarios y suposiciones que consideres oportunos sobre el desarrollo al correo electrónico : tgomariz@odilo.us
