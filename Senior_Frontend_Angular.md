ODILO Senior Frontend Angular Engineer Test
===========================================

# PRUEBA DE CÓDIGO #


Esta prueba busca evaluar mediante un pequeño caso práctico las capacidades técnicas del candiato con respecto al area Front de aplicaciónes Angular, y sobre todo las buenas prácticas de código y el diseño de interfaz, performance, y capacidad de manejo de estilos.

Es una posición que engloba la programacíon de la lógica del aplicaciones APIRest web con Angular, alojadas en el cloud e incluyendo la parte de experiencia de usuario, interfaz, estilos, maquetación, efectos.


## Tecnologías ##

Angular, últimas versiones disponibles en el momento. 

No incluyas componentes externos tal cual, y sobre todo bibliotecas de estilo similar a Bootstrap 

Puedes coger elementos de Angular Material y personalizar aspectos visuales o crear temas personalizados, anular estilos, usar librerías de iconos..

Vamos a fijarnos bastante en la apariencia de la aplicación: colores, tamaños de fuente, estilos de botones, fondos, márgenes, bordes y demás cosas como aria-labels.. que sean respetuosas con la accesibilidad.

Barras de progreso, sliders, hover... dale rienda suelta a los efectos !!

Queda de tu mano incluir o mejorar la gestión de errores, seguridad, añadir test o cualquier cosa que veas conveniente.. valoraremos ese esfuerzo adicional.




## Instrucciones de entrega ## 
El plazo es abierto, cuanto antes lo tengas antes lo revisamos. Como regla general damos una semana,para ajustarlo en tus horarios disponibles.

Cuando termines:

- Envía un ZIP con el proyecto: 
Comparte un drive con el proyecto empaquetando el código fuente {your_first_name}-{your_last_name}-frontend.zip

- O bien publicas en un repo personal y nos das acceso 

En cualquier caso, a las personas que te envíemos el mail de la práctica nos incluyes en el mail/invitación.

Mucha suerte!!


## Práctica ##

Se valorará usar Angular en últimas versiones. Revisar las condiciones indicadas en la sección de arriba para entender bien lo que se persigue y que tecnologías usar

Se pide la creación de una aplicación que ayude a obtener la lista de usuarios y perfiles de Github, explotando la API Rest pública de Github https://api.github.com/search/users?q=YOUR_NAME

Hay bastante documentación en la web, por ejemplo https://docs.github.com/en/rest?apiVersion=latest

En la home se debe incluir un campo de entrada de texto y un botón, para que se pueda pedir el usuario y recuperar la información utilizando el API indicada. Habría que incluir una condición para que mínimo se introduzcan 4 caracteres, y otra para que NO permita realizar la búsqueda de la palabra “gcpglobal”.

El servicio que recupera los datos de la api debe implementar el mecanismo de observables.

Los registros se deben mostrar en un componente paginados de 10 en 10 en algún tipo de lista a tu elección, incluyendo su nombre de usuario ('user.login'), el número de followers de cada registro y el avatar de tal forma que quede bien, a tu gusto. 

Esta llamada no requiere autenticación 
curl 'https://api.github.com/search/users?q=manu+in:login'


Cada perfil de usuario debe ser un enlace, para que al hacer clic en cada registro, navegue a una nueva pestaña del navegador y que en la ruta de la url incluya la propiedad 'user.login' como parámetro, para abrir el perfil de dicho usuario

Agregar un Guard que no permita consultar el perfil de usuarios con un 'score' menor a 20.0.

La aplicación debe ser diseñada para que que funcione en diferentes navegadores (excepto Internet Explorer) y con diferentes tamaños de ventana, que sea responsive


Opcional:

Puedes adicionalmente hacer alguno/s de estos puntos ordenado de mayor a menor importancia para la prueba:

1.Crear un carrusel con un componente tipo card en el que se vea el parámetro de la URL, y la imagen del usuario ('avatar_url') y alguna otra información que te parezca relevante. Al hace clik sobre un card del carrusel podré ver una vista detallada con algún otro detalle que consideres.

2.Crear un gráfico de barras simple para mostrar el número de seguidores de los usuarios en la home, se puede utilizar una librería de elección.

3.Incluir un componente para mostrar mensajes de Error en toda la aplicación y que esté implementado como un interceptor o middleware por bajo