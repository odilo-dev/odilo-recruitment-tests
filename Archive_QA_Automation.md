# ODILO QA Automation for Archive Team Recruitment Test

## El problema

Necesitamos automatizar pruebas de nuestra aplicacion de Portal público de nuestra aplicación. Para acceder a ésta
aplicación puedes hacerlo a través de la URL https://demo-memoriadigital.odilo.es/portalArchivo, se pide lo siguigente:

* Deben realizarse dos pruebas, una que realice una búsqueda general sobre un término cualquiera y otra que realice una
  búsqueda avanzada aplicando filtros. Todas éstas pruebas se harán sobre el "Portal Archivo"
  . ![img.png](resources/Archive_QA_Automation/img_2.png)
* En el caso de la búsqueda general se debe seleccionar uno de los resultados (lo que nos abrirá la ficha del mismo) y
  comprobar
  que tienen: Título, Nivel de Descripción y al menos una imagen adjunta.
* Tras ésto se accederá a la imagen para visualizarla (se abre un visor) y se probarán al menos dos botones de éste
  visor (a elegir por el candidato) verificando que funcionan correctamente.
* En el caso de la segunda búsqueda ésta se hará sin añadir ningún término, exclusivamente pulsando el botón "buscar".
  Esto nos creará una búsqueda sobre todos los registros públicos, entonces en la vista de resultados se seleccionará el
  filtro de "Expedientes" y comprobaremos que todos los registros devueltos son de éste
  tipo:![img.png](resources/Archive_QA_Automation/img.png)
* Además de estas pruebas, se valorará positivamente cualquier otra prueba que el candidato o candidata considere
  oportuno incluir

Se puede utilizar la tecnología que se considere oportuna, aunque nosotros recomendamos usar Cypress.

## El entregable

* El proyecto con la funcionalidad pedida que se pueda ejecutar y las instrucciones de ejecución que se consideren
  pertinentes
* Un documento de texto con la descripción de las pruebas y las consideraciones oportunas

Una vez terminado el proyecto realizar un envío con el zip con toda la documentación al correo de la persona que te
facilitó
esta prueba técnica.

Puedes consultar a la persona que te facilitó la prueba cualquier duda que tengas, estamos abiertos a responder
cualquier duda: comprendemos que el proceso puede ser un poco complejo, tómate tu tiempo y pregunta lo que consideres
oportuno

¡Mucha suerte!